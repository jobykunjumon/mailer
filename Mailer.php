<?php
/**
 * PHP end point to send mail 
 * @author Joby E Kunjumon <jobyekunjumon@gmail.com> 
 */
// include mailer class 
require_once 'Mailservice.php';

$_POST['toEmail'] = 'jobyekunjumon@gmail.com';
$_POST['subject'] = 'Test Subject';
$_POST['content'] = 'Test content';

if(isset($_POST) && $_POST) {
    // prepare mailer data. DOnt use POST data directly for security reasons
    $data['toEmail'] = isset($_POST['toEmail']) ? $_POST['toEmail'] : '';
    $data['subject'] = isset($_POST['subject']) ? addslashes($_POST['subject']) : '';
    $data['content'] = isset($_POST['content']) ? addslashes($_POST['content']) : '';
    
    // instantiate mailer class
    $mailer = new MailCore_Service_Mailservice();
    
    // invoke send method and analyse response 
    $mailerResponse = $mailer->send($data);
    
    print_r($mailerResponse);
} // end: if