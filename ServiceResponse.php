<?php

/*
 * Validator class
 * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
 *
 */

class UtilityComponents_Service_ServiceResponse  {
    
    /**
     * get service response
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input Array, fields and rules to validate
     * @return Array, with validation error, false if no errors
     */    
    public static function getResponse($status = '',$message = '',$data = '') {     
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $data;
        return $response;
    } // end: function getErrors
    
    
}
