<?php

// include validator class 
require_once 'Validator.php';

// include service response class 
require_once 'ServiceResponse.php';

// include switmailer
require_once('swiftMailer/swift_required.php');

/**
 * Core service handles mailing
 * @author Joby E Kunjumon <jobyekunjumon@gmail.com> *
 */
class MailCore_Service_Mailservice
{	
    /**
     * Private property for smtp host 
     */
    private $smtpHost = 'gmail';

    /**
     * Private property for smtp host 
     */
    private $smtpHostUrl = 'smtp.gmail.com';

    /**
     * Private property for smtp port 
     */
    private $smtpPort = 587;

    /**
     * Private property for from email 
     */
    private $smtpAuthUsername = 'imwithu.rs@gmail.com';

    /**
     * Private property for from email password 
     */
    private $smtpAuthString = 'Raj@1986!';

    /**
     * Protected property for from name 
     */
    protected $fromName = 'Company Name';
    
    /**
     * Protected property for default mailing server address
     */
    protected $defaultMailingServer = "smtp.livsites.com";
    
    /**
     * Protected property for default mailing port
     */
    protected $defaultMailingPort = 25;
    
    /**
     * Controller function to Send mail
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input array of data containing necessary and additional parameters as listed below
     *  $data['email'] : String, required 
     *  $data['subject'] : String, required 
     *  $data['content'] : String, required
     * @return Array $response
     */
    public function send($data) {   
        $validator = new UtilityComponents_Service_Validator();
        $responseService = UtilityComponents_Service_ServiceResponse;
        
        try {
            // sanitize user inputs
            $allowedInputs = ['toEmail' => 'email', 'subject' => 'string', 'content' => 'string'];
            $cleanData = $validator->getSanitizedInputData($data, $allowedInputs);

            // validate user inputs
            $validationRules = ['toEmail' => ['required' => ''], 'subject' => ['required' => '', 'stringLength' => [5, 160]], 'content' => ['required' => '']];
            $validationErrors = $validator->getErrors($cleanData, $validationRules);

            // if there is validation errors, return errors 
            if ($validationErrors) {
                // prepare response and send back 
                return $responseService::getResponse('validation_errors', 'Validation errrors', ['validation_errors' => $validationErrors]);
            }// end: if

            if ($this->sendMail($cleanData)) {
                // prepare response and send back 
                return $responseService::getResponse('success', 'Mail sent successfully');
            } // end: if
            // send default response
            return $responseService::getResponse('unknown_error', 'Could not send mail.', ['message' => 'Could not send mail.']);
        } catch (Exception $e) {
            // send exception message
            return $responseService::getResponse('unknown_error', 'Something went wrong while processing your request.Please try again later.', ['message' => 'Something went wrong while processing your request.Please try again later.']);
        }
    } // end: function send
    
    /**
     * Check whether the server has an open SMTP connection 
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input None
     * @return Bool
     */
    public function hasSmtpConnection() {
        // check whether script connects to the smtp server properly
        $hasSmtpConnection = false;
        $socketFle = @fsockopen('smtp host', 25);
        if ($socketFle !== false) {
            $res = fread($socketFle, 1024);
            if (strlen($res) > 0 && strpos($res, '220') === 0) {
                $hasSmtpConnection = true;
            }
        } // end: if
        
        return $hasSmtpConnection;
    } // end: function hasSmtpConnection
    
    
    /**
     * This function choose between different type of mailer functions based on the server configuration
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input array of data containing necessary and additional parameters as listed below
     *  $data['email'] : String, required 
     *  $data['subject'] : String, required 
     *  $data['content'] : String, required
     * @return Array $response
     */
    public function sendMail($data) {
        // get mailer transport from configurations 
        if ($this->hasSmtpConnection()) {
            return $this->sendThirdpartyMail($data['toEmail'], $data['subject'], $data['content'], '');
        } // end: if 
        
        // else try to send PHP mail
        return $this->sendPHPMail($data['toEmail'], $data['subject'], $data['content'], '');
        
    } // end : function sendMail
    
    /**
     * Send mail using third party email servers
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String $email, required, to which email to be send 
     * @input String $subject required 
     * @input String $content required
     * @input String $attachment , otpional, url of file to be attached
     * @return Bool true or false
     */
    public function sendThirdpartyMail($toEmail, $subject, $content, $attachment = '') {
        try {            
            // Create the Transport
            $transport = Swift_SmtpTransport::newInstance($this->smtpHostUrl, $this->smtpPort)
                    ->setUsername($this->smtpAuthUsername)
                    ->setPassword($this->smtpAuthString)
            ;

            // Create the Mailer using your created Transport
            $mailer = Swift_Mailer::newInstance($transport);

            // Create a message
            $message = Swift_Message::newInstance($subject)
                    ->setFrom(array($this->smtpAuthUsername => $this->fromName))
                    ->setTo(array($toEmail))
                    ->setBody($content, 'text/html')
            ;

            // Send the message
            if ($result = $mailer->send($message)) {
                return true;
            } // end: if
            
            //return default response 
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

// end function _sendMail

    /**
     * Send PHP mail
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String $email, required, to which email to be send 
     * @input String $subject required 
     * @input String $content required
     * @input String $attachment , otpional, url of file to be attached
     * @return Bool true or false
     */
    public function sendPHPMail($toEmail, $subject, $content, $attachment = '') { 
        try {
            // set headers
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= "X-Mailer: PHP/" . phpversion() . "\r\n";
            // set from address
            if (isset($this->smtpAuthEmail) && $this->smtpAuthEmail) {
                $headers .= "From: Livsites < " . $this->smtpAuthEmail . " >" . "\r\n";
                $headers .= "X-Sender: Livsites < " . $this->smtpAuthEmail . " >" . "\r\n";
            } // end: if
            
            // set mailer server if configuration exists
            if(isset($this->smtpAuthEmail) && $this->smtpAuthEmail && isset($this->defaultMailingPort) && $this->defaultMailingPort) {
                ini_set("SMTP", $this->defaultMailingServer);
                ini_set('smtp_port', $this->defaultMailingPort);
            } // end: if

            // send mail
            if (mail($toEmail, $subject, $content, $headers)) {
                return true;
            }// end: if
            
            // return default value
            return false;
        } catch (Exception $e) {
            return false;
        }
        
    }

// end: function sendPHPMail

	
}