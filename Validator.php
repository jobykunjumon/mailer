<?php

/**
 * Validator class
 * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
 *
 */

class UtilityComponents_Service_Validator  {
    // validation errors var
    protected $validationErrors;
    
    // input data
    protected $data;
    /**
     * get validation errors
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input Array, fields and rules to validate
     * @return Array, with validation error, false if no errors
     */    
    public function getErrors($data,$validationRules) {          
        if(empty($validationRules)) {
            return false;
        } // end: if
        
        $this->data = $data;
        
        foreach($validationRules as $field => $validationRule) {
            $this->validate($field, $validationRule);
        } // end: foreach
        
        return $this->validationErrors;
    } // end: function getErrors
    
    /**
     * do validation based on the given validation rule
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String, field name to be validated
     * @input Array, validation rules
     * @return None
     */   
    public function validate($field, $validationRule) { 
        foreach($validationRule as $validator => $validatorData) {
            $validatorMethod = 'validate'.ucwords($validator); 
            
            
            if($validationError = $this->$validatorMethod($field,$validatorData)) {
                $this->validationErrors[$field] = $validationError;
                break;
            } // end if
        }
    } // end: function validate
    
    public function validateStringLength($field,$data) {
        $minStrLen = $data[0] ?? 0;      
        $maxStrLen = $data[1] ?? 0; 
        
        if (strlen(trim($this->data[$field])) < $minStrLen || strlen(trim($this->data[$field])) > $maxStrLen) {
            // validate password length
            return ucwords(str_replace('_',' ',$field)). ' length should be between ' . $minStrLen . ' and ' . $maxStrLen . ' characters';
        } // end : if

        return false;
    }
    
    /**
     * Validate required field 
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String value
     * @return validation error message || Bool
     */    
    public function validateRequired($field,$validatorData) {
        if(empty(trim($this->data[$field]))) {
            return ucwords(str_replace('_',' ',$field)).' is required';
        }
        return false;
    }// end: function validateRequired
    
    /**
     * Validate current password 
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String password
     * @return validation error message || Bool
     */    
    public function validateCurrentPassword($field,$validatorData) {
        $authService = new AuthCore_Service_Authservice();
        $authResponse = $authService->getAuthenticateCurrentPassword($this->data[$field]);
        if(isset($authResponse['status']) && $authResponse['status'] == "success") {
            return false;
        } // end: if
        return "Current password is wrong";
    } // end: function validateCurrentPassword
    
    /**
     * Validate password 
     * @author Joby E Kunjumon <jobyekunjumon@gmail.com>
     * @input String password
     * @input String confirm_password
     * @return validation error message || Bool
     */    
    public function validatePasswordConfirmation($field,$validatorData) {     
        if($this->data['new_password'] != $this->data['confirm_password']) {
            return 'Password confirmation failed';
        } // 
        
        return false;
    } // end: function validatePassword
    
       
    /**     
     * Get sanitized input data     
     * @author Joby <joby.kunjumon@embitel.com>     
     * @param array $data    
     * @param array $allowedInputs      
     */    
    public function getSanitizedInputData($data, $allowedInputs) {        
        $cleanData = array();        
        // loop through the allowed inputs and check whether the values exist, and if exists, filter the value and add to output data        
        foreach ($allowedInputs as $field => $type) {            
            if (isset($data[$field]) && $data[$field]) {                
                $cleanData[$field] = $this->getFilteredValue($data[$field], $type);
            } // end: if        
        } // end: foreach 
        
        return $cleanData;    
    }
// end: function getSanitizedInputData    
    
    /**     
     * Filter values using  filter_var function     
     * @author Joby <joby.kunjumon@embitel.com>     
     * @param string $value     
     * @param string $filterType     
     */    
    public function getFilteredValue($value, $filterType) {        
        if($filterType == 'nofilter') {            
            return $value;            
        } // end : if                
        
        // create the array of necessary filters        
        $filters = array('email' => FILTER_VALIDATE_EMAIL, 'string' => FILTER_SANITIZE_STRING, 'number' => FILTER_SANITIZE_NUMBER_INT, 'ip' => FILTER_VALIDATE_IP);        
        
        // filter the value if filter key exists         
        if (isset($filters[$filterType])) {            
            return addslashes(filter_var($value, $filters[$filterType]));   
        } // end: if      
          
        // return default value        
        return false;            
    }
// end: function getFilteredValue
}
